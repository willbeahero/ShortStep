package leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * 
给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。

你可以假设每种输入只会对应一个答案。但是，你不能重复利用这个数组中同样的元素。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/two-sum
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * 
 * 
 * @author yangcheng  
 * @date 2019年10月9日  
 * @version V1.0
 */
public class Code001 {
	int[] nums = {2, 7, 11, 15};
	int target = 9;
	
	public int[] twoSum(int[] nums, int target) {
		int[] ret = new int[2];
		
		
		int len = nums.length;
		Map<Integer, Integer> cache = new HashMap<>();
		
		for(int i =0 ; i < len ; i ++){
			int value = target - nums[i];
			if(!cache.containsKey(value)){
				cache.put(nums[i], i);//下标
			}else{
				ret[0]=nums[i];
				ret[1]=cache.get(value);
			}
		}
		 
		 
		 
		return ret;
	        
	        
	        
	        
	 }

}
