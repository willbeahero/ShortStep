package leetcode.concurrent;

import java.util.concurrent.atomic.AtomicInteger;

public class Code1114 {

    public void first(Runnable printFirst) throws InterruptedException {
        
        // printFirst.run() outputs "first". Do not change or remove this line.
    	if(pointer.get()== 0 ){
    		printFirst.run();
    	}
    	pointer.set(1);
        
    }

    public void second(Runnable printSecond) throws InterruptedException {
        
        // printSecond.run() outputs "second". Do not change or remove this line.
    	while(pointer.get() != 1 ){
    		Thread.sleep(20);
	    }
    	printSecond.run();
		pointer.set(2);
    }

    public void third(Runnable printThird) throws InterruptedException {
        
        // printThird.run() outputs "third". Do not change or remove this line.
    	while(pointer.get() != 2 ){
	       Thread.sleep(20);
	    }
    	 printThird.run();
    }
    
    volatile AtomicInteger pointer = new AtomicInteger(0);
    
    
    
    public static void main(String[] args) {
		
	}
	

}
