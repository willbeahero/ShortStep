package leetcode.concurrent;


public class Code1115 {
	
	Object lock = new Object();

	int n = 6;
	volatile boolean first = true;

    public void foo(Runnable printFoo) throws InterruptedException {
        
        for (int i = 0; i < n; i++) {
            synchronized (lock) {
            	// printFoo.run() outputs "foo". Do not change or remove this line.
            	first = false;
            	printFoo.run();
            	lock.notify();
            	lock.wait();
			}
        	
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {
        
        	synchronized (lock) {
        		int i = 0 ;
        		while(i<n){
        			if(!first){
            			for (i = 0; i < n; i++) {
    	                	printBar.run();
    	                	lock.notify();
    	                	lock.wait();
            			}
            		}
            		lock.notify();
                	lock.wait();
        		}
        		
            	
        	}
        	
    }
    public void test() throws InterruptedException{
    	
    	
    	new Thread(new Runnable() {
    		
    		@Override
    		public void run() {
    			try {
    				bar(new Runnable() {
    					
    					@Override
    					public void run() {
    						System.out.println("bar");
    						}
    					});
    				} catch (InterruptedException e) {
    					e.printStackTrace();
    				}
    		}
    	}).start();
    	
	new Thread(new Runnable() {
				
		@Override
		public void run() {
			try {
				foo(new Runnable() {
					
					@Override
					public void run() {
						System.out.println("foo");
					}
				});
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}).start();
	    	
//	Thread.sleep(1000*60);
    	
    	
    	
    }
    

}
