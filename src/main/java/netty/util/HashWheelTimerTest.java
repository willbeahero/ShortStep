package netty.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;

/**
 * netty时间轮练习
 * @author yangcheng  
 * @date 2020年6月17日  
 * @version V1.0
 */
public class HashWheelTimerTest {
	
	public static void main(String[] args) {
		
		/**
		 * 初始化一个时间轮，与一个线程绑定
		 */
		HashedWheelTimer timer = new HashedWheelTimer(new ThreadFactory() {
			
			@Override
			public Thread newThread(Runnable r) {
				return new Thread(r,"时间轮线程");
			}
		}, 1, TimeUnit.SECONDS,512);
		
		/**
		 * 	创建一个netty下的TimerTask实例
		 */
		TimerTask  taskA = new TimerTask() {
			
			@Override
			public void run(Timeout timeout) throws Exception {
				
				
				System.out.println("执行一次");
				timer.newTimeout(this, 2, TimeUnit.SECONDS);
			}
		};
		/**
		 * 将
		 */
		timer.newTimeout(taskA, 2, TimeUnit.SECONDS);
		
		
	}
	

}
