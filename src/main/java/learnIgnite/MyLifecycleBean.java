package learnIgnite;

import org.apache.ignite.IgniteException;
import org.apache.ignite.lifecycle.LifecycleBean;
import org.apache.ignite.lifecycle.LifecycleEventType;

/**
 * ignite的生命周期各阶段监听器，通过捕获
 * 相应事件可以分别做相应操作
 * @author yangcheng  
 * @date 2019年8月28日  
 * @version V1.0
 */
public class MyLifecycleBean implements LifecycleBean {

	@Override
	public void onLifecycleEvent(LifecycleEventType evt) throws IgniteException {
		
		/**
		 * 一共有四种事件类型 
		 * BEFORE_NODE_START：Ignite节点的启动程序初始化之前调用
		   AFTER_NODE_START：Ignite节点启动之后调用
		   BEFORE_NODE_STOP：Ignite节点的停止程序初始化之前调用
		   AFTER_NODE_STOP：Ignite节点停止之后调用
		 */
		if (evt == LifecycleEventType.BEFORE_NODE_START) {
            System.out.println("节点启动之前执行操作......");
        }else if(evt == LifecycleEventType.BEFORE_NODE_STOP){
        	System.out.println("节点关闭之前执行操作......");
        }
	}

}
