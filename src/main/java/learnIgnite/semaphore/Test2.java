package learnIgnite.semaphore;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.IgniteSemaphore;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;

/**
 * 
 * @author yangcheng  
 * @date 2020年5月1日  
 * @version V1.0
 */
public class Test2 {

	public static void main(String[] args) {
		TcpDiscoverySpi spi = new TcpDiscoverySpi();
		TcpDiscoveryMulticastIpFinder ipFinder = new TcpDiscoveryMulticastIpFinder();
		ipFinder.setMulticastGroup("228.10.10.157");
		spi.setIpFinder(ipFinder);
		IgniteConfiguration cfg = new IgniteConfiguration();
		cfg.setDiscoverySpi(spi);
		Ignite ignite = Ignition.start(cfg);
		IgniteCluster cluster = ignite.cluster();
		IgniteSemaphore semaphore = cluster.ignite().semaphore("testSemaphore", 2, true, true);
		if(semaphore.tryAcquire()){
			System.out.println("获取到信号量");
			semaphore.release();
		}
		semaphore.close();
		
		
		
		System.out.println(semaphore.removed());
		System.out.println(semaphore.tryAcquire());
		
		
	}
}
