package learnIgnite.semaphore;

import java.util.Arrays;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.IgniteSemaphore;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;

public class Test {
	public static void main(String[] args) {
		TcpDiscoverySpi spi = new TcpDiscoverySpi();
		TcpDiscoveryMulticastIpFinder ipFinder = new TcpDiscoveryMulticastIpFinder();
		ipFinder.setMulticastGroup("228.10.10.157");
		spi.setIpFinder(ipFinder);
		IgniteConfiguration cfg = new IgniteConfiguration();
		cfg.setDiscoverySpi(spi);
		Ignite ignite = Ignition.start(cfg);
		IgniteCluster cluster = ignite.cluster();
		IgniteSemaphore semaphore = cluster.ignite().semaphore("testSemaphore", 2, true, true);
		new Thread(() -> {
			semaphore.acquire();
			System.out.println(Thread.currentThread().getName());
			try {
				Thread.sleep(1000*60);
				semaphore.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		new Thread(() -> {
			semaphore.acquire();
			System.out.println(Thread.currentThread().getName());
			try {
				Thread.sleep(1000*60);
				semaphore.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		
		new Thread(() -> {
			semaphore.acquire();
//			semaphore.close();  //关闭信号量
			System.out.println("获取到锁");
			semaphore.release();
		}).start();
		
	}
}
