package learnIgnite.cache;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;

import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;

/**
 * javax得Cache利用ignite得分布式优势
 * @author yangcheng  
 * @date 2019年8月30日  
 * @version V1.0
 */
public class Ignite4Jcache {
	
	public static void main(String[] args) {
		
		/**
		 * 下面是使用JCache管理器时，如何进行Ignite缓存配置的示例
		 */
		// Get or create a cache manager.
		CacheManager cacheMgr = Caching.getCachingProvider().getCacheManager();

		// This is an Ignite configuration object (org.apache.ignite.configuration.CacheConfiguration).
		CacheConfiguration<Integer, String> cfg = new CacheConfiguration<>();

		// Specify cache mode and/or any other Ignite-specific configuration properties.
		cfg.setCacheMode(CacheMode.PARTITIONED);

		// Create a cache based on the configuration created above.
		Cache<Integer, String> cache = cacheMgr.createCache("aCache", cfg);
		
		
	}

}
