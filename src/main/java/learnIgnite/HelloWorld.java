package learnIgnite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.IgniteLogger;
import org.apache.ignite.Ignition;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.compute.ComputeJob;
import org.apache.ignite.compute.ComputeJobAdapter;
import org.apache.ignite.compute.ComputeJobResult;
import org.apache.ignite.compute.ComputeTaskFuture;
import org.apache.ignite.compute.ComputeTaskSplitAdapter;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.lang.IgniteFuture;
import org.apache.ignite.lang.IgniteInClosure;
import org.apache.ignite.lang.IgniteRunnable;
import org.apache.ignite.logger.slf4j.Slf4jLogger;

/**
 * 
 * @author yangcheng  
 * @date 2019年8月9日  
 * @version V1.0
 */
public class HelloWorld {
	public static void main(String[] args) {
		IgniteConfiguration cfg = new IgniteConfiguration();
		
		//将事件监听注入到Ignite中
		cfg.setLifecycleBeans(new MyLifecycleBean());
		//以服务端模式启动
		cfg.setClientMode(false);
		
		//使用slf4j日志框架---首先需要导入相应的依赖包
		IgniteLogger log = new Slf4jLogger();
		cfg.setGridLogger(log);
		Ignite ignite = Ignition.start(cfg);
		
		/**
		 * 创建 name=myCache 的缓存，改创建过程会自动在所有同一集群节点上创建当前缓存
		 */
		CacheConfiguration cfg2 = new CacheConfiguration("myCache");
		IgniteCache<String,String> cache = ignite.getOrCreateCache(cfg2);
		
		cache.put("keys1", "values1");
		System.out.println(cache.get("keys1"));
		
		
		
		
		
	}
}
