package learnIgnite.cluster.simpleCluster;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;

/**
 * 默认集群方式启动Ignite节点
 * 
 * 当分布式缓存创建之后，它会自动地部署在所有的已有或者未来的服务端节点上。
 * @author yangcheng  
 * @date 2019年8月28日  
 * @version V1.0
 */
public class ServerNodeCache {
	/**
	 * 当在Ignite中创建缓存时，不管是通过XML方式，还是通过Ignite.createCache(...)
	 * 或者Ignite.getOrCreateCache(...)方法，Ignite会自动地在所有的服务端节点中部署分布式缓存。
	 * @param args
	 */
	public static void main(String[] args) {

		// Start Ignite in client mode.
		Ignite ignite = Ignition.start();

		CacheConfiguration cfg = new CacheConfiguration("myCache");

		// Set required cache configuration properties.
		// Create cache on all the existing and future server nodes.
		// Note that since the local node is a client, it will not
		// be caching any data.
		IgniteCache<String, Object> cache = ignite.getOrCreateCache(cfg);
		
		
		/**
		 * 分布式计算
		 */
		
		/**
		 * IgniteCompute默认会在所有的服务端节点上执行作业，
		 * 不过也可以通过创建相应的集群组来选择是只在服务端节点还是只在客户端节点上执行作业
		 */
		IgniteCompute compute = ignite.compute();
		
		// Execute computation on the server nodes (default behavior).
		compute.broadcast(() -> System.out.println("aaaaa.........."));
		
		compute.broadcast(() -> { 
				for(int i  = 0 ; i < 10 ; i++){
					new Thread(() -> {
						System.out.println("执行一次..........");
					}).start();
				}
			}
		);
	}
	
	

}
