package learnIgnite.cluster.di;

import java.util.Collection;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.lang.IgniteCallable;
import org.apache.ignite.resources.IgniteInstanceResource;

/**
 * 资源依赖注入测试
 * Ignite中，预定义的资源都是可以进行依赖注入的，同时支持基于属性和基于方法的注入。
 * 任何加注正确注解的资源都会在初始化之前注入相对应的任务、作业、闭包或者SPI。
 * @author yangcheng  
 * @date 2019年8月28日  
 * @version V1.0
 */

public class TestDI {
	public static void main(String[] args) {
		/**
			有很多的预定义资源可供注入：
			资源	描述
			CacheNameResource					由CacheConfiguration.getName()提供，注入网格缓存名
			CacheStoreSessionResource			注入当前的CacheStoreSession实例
			IgniteInstanceResource				注入当前的Ignite实例
			JobContextResource					注入ComputeJobContext的实例。作业的上下文持有关于一个作业执行的有用的信息。比如，可以获得包含与作业并置的条目的缓存的名字。
			LoadBalancerResource				注入ComputeLoadBalancer的实例，注入后可以用于任务的负载平衡。
			LoggerResource						注入IgniteLogger的实例，它可以用于向本地节点的日志写消息。
			ServiceResource						通过指定服务名注入Ignite的服务。
			SpringApplicationContextResource	注入Spring的ApplicationContext资源。
			SpringResource						从Spring的ApplicationContext注入资源，当希望访问在Spring的ApplicationContext XML配置中指定的一个Bean时，可以用它。
			TaskContinuousMapperResource		注入一个ComputeTaskContinuousMapper的实例，持续映射可以在任何时点从任务中发布作业，即使过了map的初始化阶段。
			TaskSessionResource					注入ComputeTaskSession资源的实例，它为一个特定的任务执行定义了一个分布式的会话。
		 */
		
		Ignite ignite = Ignition.start();
//		Ignite ignite = Ignition.ignite();

		/**
		 * IgniteCallable接口继承了jdk的Callable接口所以你懂得--有返回值 异步
		 */
		Collection<String> res = ignite.compute().broadcast(new IgniteCallable<String>() {
		  //通过注解的方式注入资源
		  @IgniteInstanceResource//注入当前的Ignite实例--作用有点类似spring的@autowired
		  private Ignite ignite;

		  @Override
		  public String call() throws Exception {
		    IgniteCache<Object, Object> cache = ignite.getOrCreateCache("Local_Cache");
			return "success";
		  }
		});
		for (String string : res) {
			System.out.println(string);
		}
		
	}

}
