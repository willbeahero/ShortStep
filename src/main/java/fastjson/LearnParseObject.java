package fastjson;

import com.alibaba.fastjson.JSON;

public class LearnParseObject {
	public static void main(String[] args) {
		A a = new A("nihao");
		String str = JSON.toJSONString(a);
		B b = new B();
		b = JSON.parseObject(str, B.class);
		System.out.println(b);
	}
}

