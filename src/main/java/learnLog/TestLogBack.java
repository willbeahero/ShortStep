package learnLog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * SLF4J作为门面，logBack是log4j的升级版本
 * @author yangcheng  
 * @date 2019年8月23日  
 * @version V1.0
 */
public class TestLogBack {
	protected static final Logger  logger = LoggerFactory.getLogger(TestLogBack.class); 
	public static void main(String[] args) {
		
		logger.debug("输出DEBUG级别的日志"); 
        logger.info("输出INFO级别的日志"); 
        logger.error("输出ERROR级别的日志"); 
		
	}

}
