package kafka;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;



public class MyKafkaConsumer {
	
	public static void  init(){
		Properties props = new Properties();
        //集群地址，多个地址用"，"分隔
        props.put("bootstrap.servers","192.168.18.27:9092");
        //设置消费者的group id
        props.put("group.id", "msgTransWorker_DN");
        //如果为真，consumer所消费消息的offset将会自动的同步到zookeeper。如果消费者死掉时，由新的consumer使用继续接替
        props.put("enable.auto.commit", "true");
        //consumer向zookeeper提交offset的频率
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        //反序列化
        props.put("key.deserializer", StringDeserializer.class.getName());
        props.put("value.deserializer", StringDeserializer.class.getName());
        //创建消费者
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        // 订阅topic，可以为多个用,隔开，此处订阅了"test-partition-1", "test"这两个主题
        consumer.subscribe(Arrays.asList("testTopic"));
        //持续监听
        while(true){
            /**
             * poll频率
             * poll自动在批量消费数据
             */
            ConsumerRecords<String,String> consumerRecords = consumer.poll(100);
            for(ConsumerRecord<String,String> consumerRecord : consumerRecords){
                     System.out.println("在test-partition-1中读到：" + consumerRecord.value());
            }
        }
	}
	
    public static void main(String[] args) {
    	init();
    }
}
