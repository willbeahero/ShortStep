package TDengine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * 测试批量插入单条数据性能——1秒插入记录值为两千条（每条数据为一个insert）
 * @author yangcheng  
 * @date 2020年4月15日  
 * @version V1.0
 */
public class Test002 {
	public static Connection getConnection() throws ClassNotFoundException, SQLException{
		Class.forName("com.taosdata.jdbc.TSDBDriver");
		String jdbcUrl = "jdbc:TAOS://yc.tdengine.com:6030/log?user=root&password=taosdata";
		Connection conn = DriverManager.getConnection(jdbcUrl);
		
		return conn;
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Connection connection = getConnection();
		Statement stmt = connection.createStatement();
		// create database
		stmt.executeUpdate("create database if not exists eiot_db");
		// use database
		stmt.executeUpdate("use eiot_db");
		
		/**
		 * 创建超级表 同时创建超级表的普通表
		 */
//		stmt.executeUpdate("create table if not exists ycl_super_db (ts timestamp, measure_id BINARY(32),"
//				+ " device_id BINARY(32), station_id BINARY(32) , item_tag BINARY(16),"
//				+ " item_value  FLOAT,  item_time  timestamp , regularity_time timestamp, msg_desc BINARY(1), data_type TINYINT,gmt_create_regularity timestamp"
//				+ " ) tags (intval_clc  TINYINT ,  proId BINARY(32))");//tag 为数据周期 和  产品id
//		
//		stmt.executeUpdate("create table  if not exists meter01 using ycl_super_db tags (5,'000000001')");
		
		
		// insert data
		String insertData_1 = "insert into meter01 values(now,'measuere0000111' ,"
				+ "'divice00000001','station0000001','Ua',"
				+ "23.11 , now ,now ,'h','1',now )";
		long start = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
		/**
		 * 1秒插入记录值为两千条
		 * 
		 */
		
		
		for(int i = 0 ; i < 100000 ; i ++){
			stmt.executeUpdate(insertData_1);
		}
		
		
		
		System.out.println("insert 1W条数据花费时间 = " + (LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8")) - start));
		
//		ResultSet set = stmt.executeQuery("select * from meter01");
//		while(set.next()){
//			System.out.println(set.getTimestamp(1)+" , "+ set.getString(2));
//		}
		
		stmt.close();
		connection.close();
	}
}
