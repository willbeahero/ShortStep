package influxdb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;
import org.influxdb.dto.QueryResult.Series;

import influxdb.util.InfluxDBConnection;
/**
 * 从
 * @author yangcheng  
 * @date 2019年11月28日  
 * @version V1.0
 */
public class TestSelect {
	public static void main(String[] args) {
		InfluxDBConnection influxDBConnection = new InfluxDBConnection("admin", "admin", "http:\\192.168.219.130:8086","pastoralDogDB", "30day");
		/**
		 *where语句可使用比较运算符或者正则表达式
			使用比较运算符当比较对象是field且值是字符串类型时必须使用单引号
			使用比较运算符当比较对象是tag时必须使用单引号
			使用正则表达式时必须没有单引号
		 */
		QueryResult results = influxDBConnection
				//.query("SELECT * FROM measurements001 where tag1 = '标签值'  order by time desc limit 1000");
				.query("SELECT * FROM measurements001 where field2 = 3.141592657 order by time desc limit 1000");//
		//results.getResults()是同时查询多条SQL语句的返回值，此处我们只有一条SQL，所以只取第一个结果集即可。
		Result oneResult = results.getResults().get(0);
		if (oneResult.getSeries() != null) {
			List<List<Object>> valueList = oneResult.getSeries().stream().map(Series::getValues)
					.collect(Collectors.toList()).get(0);
			if (valueList != null && valueList.size() > 0) {//valueList == [[2019-11-28T09:58:38.628Z, String类型, 3.151592657, 标签值], [2019-11-28T09:58:48.077Z, String类型, 3.151592657, 标签值]]
				for (List<Object> value : valueList) {
					Map<String, String> map = new HashMap<String, String>();
					// 数据库中字段1取值
					String field1 = value.get(0) == null ? null : value.get(0).toString();
					// 数据库中字段2取值
					String field2 = value.get(1) == null ? null : value.get(1).toString();
					// TODO 用取出的字段做你自己的业务逻辑……
					System.out.println("field1="+field1+" ; field2="+field2);
				}
			}
		}
		influxDBConnection.close();
	}
}
