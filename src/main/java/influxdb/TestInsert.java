package influxdb;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import influxdb.util.InfluxDBConnection;

/**
 * influxdb时序数据库练习，influxdb使用的是docker镜像部署
 * 
 * 循环10000次 需要162秒  ，，  每秒可插入62条数据
 * 
 * @author yangcheng  
 * @date 2019年11月28日  
 * @version V1.0
 */
public class TestInsert {
	public static void main(String[] args) {
		/**
		 * 变更“数据保留策略”
		 * ---数据保留30天，备份数为1   default--是否是默认策略
		 * CREATE RETENTION POLICY "30day" ON "pastoralDogDB" DURATION 720h REPLICATION 1 DEFAULT
		 * 
		 * 在插入新数据时，tag、field和timestamp之间用空格分隔。
		 */
		InfluxDBConnection influxDBConnection = new InfluxDBConnection("admin", "admin", "http:\\192.168.219.130:8086", "pastoralDogDB", null);
		Map<String, String> tags = new HashMap<String, String>();
		tags.put("tag1", "标签值");
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("field1", "String类型");
		// 数值型，InfluxDB的字段类型，由第一天插入的值得类型决定
		fields.put("field2", 3.151592657);
		// 时间使用毫秒为单位
		long start = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
		for(int i = 0 ; i < 1000 ; i++){
			influxDBConnection.insert("measurements001", tags, fields, System.currentTimeMillis(), TimeUnit.MILLISECONDS);
		}
		System.out.println("insert 1W条数据花费时间 = " + (LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8")) - start));
		
		influxDBConnection.close();
	}
}
