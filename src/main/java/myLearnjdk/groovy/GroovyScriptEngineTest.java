package myLearnjdk.groovy;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import groovy.lang.Binding;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

/**
 * 生产环境中一般使用本记载机制
 * groovy.util.GroovyScriptEngine能够处理任何 Groovy 代码的动态编译与加载，
 * 可以从统一的位置加载脚本，并且能够监听脚本的变化，当脚本发生变化时会重新加载 
 * @author yangcheng  
 * @date 2019年7月8日  
 * @version V1.0
 */
public class GroovyScriptEngineTest {
	public static void main(String[] args) throws IOException, ResourceException, ScriptException, InterruptedException {
		GroovyScriptEngine scriptEngine = new GroovyScriptEngine("D:\\");
		Binding binding = new Binding();
		binding.setVariable("name", "groovy");
		while (true){
			/**
			 * 当更改脚本文件中的代码hello 为hi时 控制台输出的内容会随之变化
			 */
		    scriptEngine.run("hello.groovy", binding);//hello.groovy脚本文件中就只有一行代码：println "hi $name"
		    TimeUnit.SECONDS.sleep(1);
		}
		
	}

}
