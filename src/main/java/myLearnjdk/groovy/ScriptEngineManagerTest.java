package myLearnjdk.groovy;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

/**
 * JDK提供的用于加载脚本文件的脚本引擎
 * 该脚本引擎不止支持groovy一种脚本语言
 * @author yangcheng  
 * @date 2019年7月9日  
 * @version V1.0
 */
public class ScriptEngineManagerTest {
	public static void main(String[] args) throws ScriptException {
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("groovy");
		Bindings bindings = new SimpleBindings();
		bindings.put("age", 22);
		Object value = engine.eval("if(age < 18){'未成年'}else{'成年'}",bindings);
		
		System.out.println(value);
	}
	
}
