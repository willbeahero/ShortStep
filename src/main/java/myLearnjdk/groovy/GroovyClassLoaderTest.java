package myLearnjdk.groovy;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.tools.ant.taskdefs.Concat;
import org.codehaus.groovy.control.CompilationFailedException;

import groovy.lang.GroovyClassLoader;
import groovy.lang.Script;

/**
 * GroovyClassLoader加载groovy脚本语言
 * 
 * groovy.lang.GroovyClassLoader是一个定制的类加载器，可以在运行时加载 Groovy 代码，生成 Class 对象。
 * 
 * gooClassLoader.parseClass每执行一次 都会重新创建一个class，因此长久调用之后会导致内存溢出
 * @author yangcheng  
 * @date 2019年7月8日  
 * @version V1.0
 */
public class GroovyClassLoaderTest {
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException, CompilationFailedException, IOException{
		GroovyClassLoader gooClassLoader = new GroovyClassLoader();
		
		/**
		 * 一字符串形式加载groovy类
		 */
//		Class clazz = gooClassLoader.parseClass("class Hello {void say(){System.out.println( 'helloworld groovy!')} }");
		
		/**
		 * 从指定文件中加载groovy类
		 * 
		 * Hello文件中内容就是class Hello {void say(){System.out.println( 'helloworld groovy!')} }
		 */
		Class clazz = gooClassLoader.parseClass(new File("D:/Hello"));
		clazz.getMethod("say").invoke(clazz.newInstance());
	}
	

}
