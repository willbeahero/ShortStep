package myLearnjdk.groovy;

import groovy.lang.GroovyShell;
import groovy.lang.Script;

/**
 * groovy.lang.GroovyShell除了可以执行 Groovy 代码外，提供了丰富的功能，
 * 比如可以绑定更多的变量，从文件系统、网络加载代码等
 * @author yangcheng 
 * @date 2019年7月8日  
 * @version V1.0
 */
public class GroovyShellTest {
	public static void main(String[] args) {
		GroovyShell groovyShell = new GroovyShell();
		
		groovyShell.setVariable("age", 12);
		
		/**
		 * 执行并返回脚本执行结果
		 * 执行1000次需要4秒左右！
		 */
		Object returnVal = groovyShell.evaluate("if(age > 18){'成年'}else{'未成年'}");
//		System.out.println(returnVal);
		
		long start = System.currentTimeMillis();
		/**
		 * 执行1000次需要3.5秒左右！
		 */
		Script script = groovyShell.parse("if(age < 18){'未成年'}else{'成年'}");
		for(int i = 1; i < 1000000 ; i++){
			
			script.run();//执行脚本得run方法 并返回结果----该方法执行效率非常高  100万次25ms左右
		}
		
		
		
		//可以从更多位置加载/执行脚本
		//shell.evaluate(new File("script.groovy"));
		//shell.evaluate(new URI("http://wwww.a.com/script.groovy"));
		
		
		System.out.println("执行了"+(System.currentTimeMillis() - start )+"ms");;
	}

}
