package myLearnjdk.redis.jedisPool;

import java.net.URI;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import redis.clients.jedis.JedisPool;

/**
 * 自定义redis连接池
 * 
 * JedisPool内部使用的是org.apache.commons.pool2.impl.GenericObjectPool.GenericObjectPool类实现的连接池
 * 由于JedisPool初始化时，使用的配置参数对象GenericObjectPoolConfig参数为默认，因此通过继承JedisPool，
 * 实现自定义配置参数实例化JedisPool
 * @author yangcheng
 * @date 2019年6月24日  
 * @version V1.0
 */
public class MyJedisPool extends JedisPool{

	public MyJedisPool(GenericObjectPoolConfig poolConfig, String host, int port) {
		super(poolConfig, host, port);
	}
	
	

}
