package myLearnjdk.redis.jedisCluster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

/**
 * redisCluster中，查看源码，每个节点都对应一个jedisPool，即每个节点得连接都是通过连接池管理得
 * @author yangcheng  
 * @date 2019年6月24日  
 * @version V1.0
 */
public class MyRedisCluster {
	
	private JedisCluster jedisCluster;
	
	
	 public MyRedisCluster() {
		super();
		int MaxIdle = 100;
		int MaxTotal = 100;
		long MaxWaitMillis = 1000;
		int MinIdle = 3;
		boolean TestOnBorrow = true;
		boolean TestOnReturn = false;
		Set<HostAndPort> nodes = new HashSet<HostAndPort>();
		JedisPoolConfig config = new JedisPoolConfig();
		nodes.add(new HostAndPort("172.30.92.76",7000));
		nodes.add(new HostAndPort("172.30.92.76",7001));
		config.setMaxIdle(MaxIdle);
		config.setMaxTotal(MaxTotal);
		config.setMaxWaitMillis(MaxWaitMillis);
		config.setMinIdle(MinIdle);
		config.setTestOnBorrow(TestOnBorrow);
		config.setTestOnReturn(TestOnReturn);
 
		jedisCluster = new JedisCluster(nodes, 5000, 5000, 5,"hyit@zhny_redis", config);
		System.out.println(jedisCluster.get("EQID::050000000001::2"));;
		try {
			jedisCluster.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	 
	public static void main(String[] args) {
		new MyRedisCluster();
	}
	

}
