package myLearnjdk.jdk.util.function;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * function包的Consumer，该接口作用：消费某个对象
 * @author yangcheng  
 * @date 2020年12月27日  
 * @version V1.0
 */
public class ConsumerTest {
	/**
	 * 开发者调用ArrayList.forEach时，一般希望自定义遍历的消费逻辑，比如：输出日志或者运算处理等。
		处理逻辑留给使用者，使用灵活多变。
		多变的逻辑能够封装成一个类（实现Consumer接口），将逻辑提取出来。
	 * @param args
	 */
	 public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<>();
        String[] prefix = {"A", "B"};
        for (int i = 1; i <= 10; i++){
        	employees.add(new Employee(prefix[i % 2] + i, i * 1000));
        }
        /**
         * 集合的forEach方法需要传入一个Consumer实现类对象
         */
//        employees.forEach(new MyConsumer());
        /**
         * 通过lamda方式也可遍历
         */
        employees.forEach((t) -> {System.out.println(t.toString());} );
    }
	 /**
	  * 雇员，包含姓名和工资
	  * @author yangcheng  
	  * @date 2020年12月27日  
	  * @version V1.0
	  */
	 static class Employee {
	     private String name;
	     private int salary;

	     public Employee() {
	         this.salary = 4000;
	     }

	     public Employee(String name, int salary) {
	         this.name = name;
	         this.salary = salary;
	     }

	     public String getName() {
	         return name;
	     }

	     public void setName(String name) {
	         this.name = name;
	     }

	     public int getSalary() {
	         return salary;
	     }

	     public void setSalary(int salary) {
	         this.salary = salary;
	     }

	     @Override
	     public String toString() {
	         return new StringBuilder()
	                 .append("name:").append(name)
	                 .append(",salary:").append(salary)
	                 .toString();
	     }
	 }
	 /**
	  * 定义MyConsumer类，该类实现Consumer接口，泛型用于指定当前类消费的类型
	  * 
	  * @author yangcheng  
	  * @date 2020年12月27日  
	  * @version V1.0
	  */
	 static class MyConsumer implements Consumer<Employee>{

	 	@Override
	 	public void accept(Employee t) {
	 		System.out.println(t.toString());
	 		
	 	}
	 	
	 }

}
