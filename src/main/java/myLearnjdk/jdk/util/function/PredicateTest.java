package myLearnjdk.jdk.util.function;

import java.util.ArrayList;
import java.util.function.Predicate;

import myLearnjdk.jdk.util.function.ConsumerTest.Employee;

/**
 * Predicate作用：判断对象是否符合某个条件
 * @author yangcheng  
 * @date 2020年12月27日  
 * @version V1.0
 */
public class PredicateTest {
	 public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<>();
        String[] prefix = {"A", "B"};
        for (int i = 1; i <= 10; i++){
        	employees.add(new Employee(prefix[i % 2] + i, i * 1000));
        }
        /**
         * 通过lamda方式也可遍历
         */
        employees.forEach((t) -> {
        	if(new SalaryPredicate(2000).test(t)){
        		System.out.println("当前员工的工资等于2000；"+t.toString());
        	}
        	
        } );
    }

}
/**
 * 通过实现Predicate接口，将一个业务逻辑从代码中抽离出来
 * @author yangcheng  
 * @date 2020年12月27日  
 * @version V1.0
 */
class SalaryPredicate implements  Predicate<Employee>{
    private int tax;

    public SalaryPredicate(int tax) {
        this.tax = tax;
    }

    /**
     * 当传入的雇员工资数等于tax时，返回true
     */
    @Override
    public boolean test(Employee employee) {
        return employee.getSalary() == tax;
    }
}