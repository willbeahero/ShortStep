package myLearnjdk.jdk.util.function;

import java.util.ArrayList;
import java.util.function.Function;

import myLearnjdk.jdk.util.function.ConsumerTest.Employee;

/**
 * Function作用：实现一个”一元函数“，即传入一个值经过函数的计算返回另一个值<br>
 * <br>
 * 使用场景：
 * V HashMap.computeIfAbsent(K , Function<K, V>) // 简化代码，如果指定的键尚未与值关联或与null关联，使用函数返回值替换。<br>
	<R> Stream<R> map(Function<? super T, ? extends R> mapper); // 转换流
 * @author yangcheng  
 * @date 2020年12月27日  
 * @version V1.0
 */
public class FunctionTest {
	public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<>();
        String[] prefix = {"A", "B"};
        for (int i = 1; i <= 10; i++){
        	employees.add(new Employee(prefix[i % 2] + i, i * 1000));
        }
        employees.forEach((t) -> {
        	System.out.println("获取到工资为；"+new MyFunction().apply(t));;
        });
        
	}
}
/**
 * 自己定义并实现一个function
 * @author yangcheng  
 * @date 2020年12月27日  
 * @version V1.0
 */
class MyFunction implements Function<Employee, Integer>{

	@Override
	public Integer apply(Employee t) {
		
		return t.getSalary();
	}
	
}


