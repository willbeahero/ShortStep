package myLearnjdk.jdk.util.function;

import java.util.function.Supplier;

import myLearnjdk.jdk.util.function.ConsumerTest.Employee;

/**
 * Supplier作用：创建一个对象（Supplier作为工厂类得作用）
 * @author yangcheng  
 * @date 2020年12月27日  
 * @version V1.0
 */
public class SupplierTest {
	public static void main(String[] args) {
		MySupplier mySupplier = new MySupplier();
		
		
		Employee employee1 = mySupplier.get();
		Employee employee2 = mySupplier.get();
		
		
		/**
		 * 使用lamda方式
		 * 
		 */
		Supplier<Employee> supplier = () -> {return new Employee();};
		Employee employee11 = supplier.get();
	}

}
/**
 * 自定义一个Supplier类，用于创建Employee对象
 * @author yangcheng  
 * @date 2020年12月27日  
 * @version V1.0
 */
class MySupplier implements Supplier<Employee>{

	@Override
	public Employee get() {
		return new Employee();
	}
	
}
