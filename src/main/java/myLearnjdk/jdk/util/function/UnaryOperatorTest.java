package myLearnjdk.jdk.util.function;

import java.util.function.Supplier;
import java.util.function.UnaryOperator;

import myLearnjdk.jdk.util.function.ConsumerTest.Employee;

/**
 * UnaryOperator作用：UnaryOperator继承了Function，与Function作用相同
	不过UnaryOperator，限定了传入类型和返回类型必需相同
 * @author yangcheng  
 * @date 2020年12月27日  
 * @version V1.0
 */
public class UnaryOperatorTest {
	public static void main(String[] args) {
		/**
		 * 通过Supplier类工厂，创建Employee对象
		 */
		Supplier<Employee> supplier = () -> {return new Employee();};
		Employee e  = supplier.get();
		e.setName("name");
		/**
		 * 调用MyUnaryOperator
		 */
		new MyUnaryOperator().apply(e);//name属性值会被替换
		System.out.println(e.getName());
	}
}
/**
 * 自定义UnaryOperator实现类
 * @author yangcheng  
 * @date 2020年12月27日  
 * @version V1.0
 */
class MyUnaryOperator implements UnaryOperator<Employee>{

	/**
	 * 与function不同点在于，返回值与参数必须是一样得
	 */
	@Override
	public Employee apply(Employee t) {
		t.setName("new Name");
		return t;
	}
	
}