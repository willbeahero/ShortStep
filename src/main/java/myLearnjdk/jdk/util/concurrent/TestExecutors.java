package myLearnjdk.jdk.util.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 测试自定义线程池
 * @author yangcheng  
 * @date 2019年11月27日  
 * @version V1.0
 */
public class TestExecutors {
	
	public static void main(String[] args) throws InterruptedException {
		ExecutorService service = Executors.newFixedThreadPool(2);
		
		service.execute(new Runnable() {
			
			@Override
			public void run() {
				while(true){
					
//							TimeUnit.SECONDS.sleep(1);
					if(Thread.currentThread().interrupted()){
						System.out.println(Thread.currentThread().getName()+"end。。。。。。。。。");
						break;
					}
					System.out.println(Thread.currentThread().getName()+"执行一次");
				}
			}
		});
		service.execute(new Runnable() {
			
			@Override
			public void run() {
				while(true){
					
//							TimeUnit.SECONDS.sleep(1);
					if(Thread.currentThread().interrupted()){
						System.out.println(Thread.currentThread().getName()+"end。。。。。。。。。");
						break;
					}
					System.out.println(Thread.currentThread().getName()+"执行一次");
				}
			}
		});
		Thread.sleep(1000);
		service.shutdownNow();
		
		
		
	}

}
