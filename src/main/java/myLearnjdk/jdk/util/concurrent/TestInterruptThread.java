package myLearnjdk.jdk.util.concurrent;
/**
 * @author yangcheng  
 * @date 2018年6月11日  
 * @version V1.0
 */
public class TestInterruptThread{
	
	
	public static void main(String[] args) throws InterruptedException {
		MyThread mtd = new MyThread();
		Thread th = new Thread(mtd);
		th.start();
		Thread.sleep(1000);
		th.interrupt();
		

	}
	


}
class MyThread implements Runnable{
	
	@Override
	public void run() {
		while(true){
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				
//				e.printStackTrace();
//				break;
//			}
			if(Thread.currentThread().isInterrupted()){
				System.out.println("ִ执行被打断....");
				break;
			}
			
			System.out.println("Running ......");
		}
		
		
	}
	
}