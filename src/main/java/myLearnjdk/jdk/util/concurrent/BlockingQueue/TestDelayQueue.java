package myLearnjdk.jdk.util.concurrent.BlockingQueue;

import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * 延时队列模拟网吧上机
 * @author yangcheng  
 * @date 2019年8月23日  
 * @version V1.0
 */
public class TestDelayQueue implements Runnable {
	
	 private DelayQueue<Wangmin> queue = new DelayQueue<Wangmin>();  
	    //默认为营业
	    public boolean yinye =true;  
	    //上机
	    public void shangji(String name,String id,int money){  
	        Wangmin man = new Wangmin(name, id, 1000 * money + System.currentTimeMillis());  
	        System.out.println("网名"+man.getName()+" 身份证"+man.getId()+"交钱"+money+"块,开始上机...");  
	        this.queue.add(man);  
	    }  
	     //下机
	    public void xiaji(Wangmin man){  
	        System.out.println("网名"+man.getName()+" 身份证"+man.getId()+"时间到下机...");  
	    }  
	  
	    @Override  
	    public void run() {  
	        while(yinye){  
	            try {  
	            	//从队列中获取值（take()方法为阻塞方法）
	                Wangmin man = queue.take();  
	                xiaji(man);  
	            } catch (InterruptedException e) {  
	                e.printStackTrace();  
	            }  
	        }  
	    }  
	      
	    public static void main(String args[]){  
	        try{  
	            System.out.println("网吧开始营业");  
	            TestDelayQueue siyu = new TestDelayQueue();  
	            Thread shangwang = new Thread(siyu);  
	            //线程启动 开始从delayqueue中take值
	            shangwang.start();  
	            //向对列中存入值
	            siyu.shangji("路人甲", "123", 3);  
	            siyu.shangji("路人乙", "234",3);  
	            siyu.shangji("路人丙", "345", 9);  
	            siyu.shangji("路人1", "1231", 20);  
	            siyu.shangji("路人2", "2342",10);  
	            siyu.shangji("路人3", "3453", 1);  
	        }  
	        catch(Exception e){  
	            e.printStackTrace();
	        }  
	  
	    }  


}


class Wangmin implements Delayed {
    
    private String name;  
    //身份证  
    private String id;  
    //截止时间  
    private long endTime;  
    //定义时间工具类
    private TimeUnit timeUnit = TimeUnit.SECONDS;
      
    public Wangmin(String name,String id,long endTime){  
        this.name=name;  
        this.id=id;  
        this.endTime = endTime;  
    }  
      
    public String getName(){  
        return this.name;  
    }  
      
    public String getId(){  
        return this.id;  
    }  
      
    /** 
     * 用来判断是否到了截止时间 
     */  
    @Override  
    public long getDelay(TimeUnit unit) { 
        //return unit.convert(endTime, TimeUnit.MILLISECONDS) - unit.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    	return endTime - System.currentTimeMillis();
    }  
  
    /** 
     * 相互比较排序用 
     */  
    @Override  
    public int compareTo(Delayed delayed) {  
    	Wangmin w = (Wangmin)delayed;  
        return this.getDelay(this.timeUnit) - w.getDelay(this.timeUnit) > 0 ? 1:0;  
    }
}
