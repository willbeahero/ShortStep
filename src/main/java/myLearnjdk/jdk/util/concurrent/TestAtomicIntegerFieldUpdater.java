package myLearnjdk.jdk.util.concurrent;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * 原子更新  类整型的字段 的更新器
 * @author yangcheng  
 * @date 2019年8月18日  
 * @version V1.0
 */
public class TestAtomicIntegerFieldUpdater {
	public static void main(String[] args) {
		/**
		 * 初始化一个指定类的指定属性的原子更新器
		 */
		AtomicIntegerFieldUpdater<Student> studentAtomicUpdater = AtomicIntegerFieldUpdater.newUpdater(Student.class, "age");
		Student stu = new Student(1);
		for(int i = 0 ; i < 100 ; i++){
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					int curAge = studentAtomicUpdater.addAndGet(stu, 1);
					System.out.println("更新之后年龄："+curAge);
				}
			}).start();
		}
		
	}
	

}


/**
 * 学生类，声名一个age成员变量，用于原子更新。该成员变量必须是
 * volatile并且是基本类型
 * @author yangcheng  
 * @date 2019年8月18日  
 * @version V1.0
 */
class Student{
	//年龄---由于原子化操作是通过反射实现的，因此边能不能是private的
	public volatile int age;

	public Student(int age) {
		super();
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
	
}