package myLearnjdk.jdk.util;

import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;


/**
 * base64编码练习
 * @author yangcheng  
 * @date 2019年9月26日  
 * @version V1.0
 */
public class Base64Test {
	public static void main(String[] args) {
		/**
		 * Base64字符串处理   StringUtil中的工具方法 与JDK自带的Base64作用一样 可以互用！！
		 */
		//jdk自带得解析服务  字符串长度为4的整数倍
		Decoder jdkDecode = Base64.getDecoder();
		byte[] jdkByte = jdkDecode.decode("yang");
		
		Encoder jdkEncode = Base64.getEncoder();
		String jdkStr = new String (jdkEncode.encode(jdkByte));
		
		System.out.println(jdkStr);

	}
}
