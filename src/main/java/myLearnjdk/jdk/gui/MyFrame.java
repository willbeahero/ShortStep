package myLearnjdk.jdk.gui;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class MyFrame {
	public static void main(String[] args) {
		JFrame f = new JFrame("104上行报文解析工具 杨承制作");
		//按钮
		JButton setupButton = new JButton("点击翻译");
	    
	    JPanel buttonPanel = new JPanel();
	    JLabel textLabel = new JLabel();
		//主面板
	    JPanel mainPanel = new JPanel();
	    
		f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
		
		
		/**
		 * 翻译按钮
		 */
		buttonPanel.add(setupButton);
		buttonPanel.setLayout(new GridLayout(1,0));
		buttonPanel.setBounds(10,  60,380,50);
		
        textLabel.setText("第一个文本框中输入上行报文：");
        textLabel.setBounds(10,10,30,20);
        mainPanel.add(buttonPanel);
        mainPanel.add(textLabel);
        
        /**
         * 文本输入框
         */
        JTextArea textarea = new JTextArea();
        textarea.setLineWrap(true);
//	        b.setBounds(150, 120 + 30, 80, 30);
        textarea.setBounds(10,  60, 700, 120);
		
        setupButton.addActionListener(new setupButtonListener(textarea,null));
        


        
        
        
        f.add(textarea);
        f.getContentPane().add(mainPanel, BorderLayout.CENTER);
        f.setSize(new Dimension(720,840));
        f.setVisible(true);
		
	}
	
	
	
	
	 
}


/**
 * 翻译按钮的监听
 * @author yangcheng  
 * @date 2019年5月29日  
 * @version V1.0
 */
class setupButtonListener implements ActionListener {

	private JTextArea source;
	private JTextArea dest;
	
    public setupButtonListener(JTextArea source, JTextArea dest) {
		super();
		this.source = source;
		this.dest = dest;
	}



	public void actionPerformed(ActionEvent e){

//        System.out.println("Setup Button pressed !"+source.getText());      
        //else if state != INIT then do nothing
    }
}
