package myLearnjdk.jdk.text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

public class TestSimpleDateFormat {
	public static void main(String[] args) throws ParseException, InterruptedException {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		
		String str = format.format(format.parse("3000-12-12 23:32:00"));
		System.out.println(str);
		
		/**
		 * 关于时间精确到毫秒级
		 */
		long initVal = System.currentTimeMillis();
		System.out.println("系统时间="+initVal);
		/**
		 * 时间值转成long值之后  毫秒值丢失！
		 */
//		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/**
		 * 不会丢失毫秒值
		 */
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S");
		Date date = new Date(initVal);		
		
		Long val = format2.parse(format2.format(date)).getTime();
		System.out.println("还原时间="+val);
		System.out.println(format2.format(date));
		
		
		
		//*******************************测试SimpleDateFormat并发性，如果不加同步 会报错**********************************
		long concurrentStartTime = System.currentTimeMillis();
		final CountDownLatch countDownLatch = new CountDownLatch(6000);
		Object obj = new Object();
		for(int i = 0 ; i <6000 ; i++){
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					long val = 0;
					try {
						/**
						 * SimpleDateFormat是非线程安全的，因此并发环境下必须同步处理
						 * ---处理并发异常方式有2种：1.使用局部变量替代成员变量。2.使用synchronized。
						 * 两种方式性能在6000并发左右无明显差异
						 */
//						synchronized (obj) {
							SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S");
							val = format2.parse(format2.format(date)).getTime();
//						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
					System.out.println("还原时间="+val);
					System.out.println(format2.format(date));
					countDownLatch.countDown();
				}
			}).start();
			
		}
		countDownLatch.await();
		System.out.println("并发时间格式转换耗时=="+(System.currentTimeMillis()-concurrentStartTime));
		
		
	}

}
