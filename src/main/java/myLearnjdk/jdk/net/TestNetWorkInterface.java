package myLearnjdk.jdk.net;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * NetWorkInterface类测试
 * @author yangcheng  
 * @date 2020年7月29日  
 * @version V1.0
 */
public class TestNetWorkInterface {
	public static void main(String[] args) throws SocketException {
		getAllIp() ;
	}

	public static void getAllIp() throws SocketException{
		/**
		 * 获取所有的网络接口
		 */
		Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
		while(interfaces.hasMoreElements()){
			NetworkInterface inface = interfaces.nextElement();
			/**
			 * 从网络接口中获取所有的地址
			 */
			Enumeration<InetAddress> adderesss = inface.getInetAddresses();
			while(adderesss.hasMoreElements()){
				/**
				 * 打印地址信息
				 */
				System.out.println(adderesss.nextElement());
			}
//			System.out.println(inface);
		}
		
	}
}
