package myLearnjdk.jdk.net;

import java.io.IOException;
import java.net.Inet6Address;

public class IPV6_Inet6Address {
	public static void main(String[] args) throws IOException {
		
		/**
		 * 将IPV6直接转成byte[]数组
		 */
		//fe80::14d8:a209:89e6:c162%14
		byte[] address = Inet6Address.getByName("fe80::14d8:a209:89e6:c162").getAddress();
		
		System.out.println("字节数组转ip："+Inet6Address.getByAddress(address).getHostAddress());;
		
		for (int i = 0; i < address.length; i++) {
			System.out.println(address[i]&0xFF);
		}
		
		
		System.out.println("getHostAddress=="+Inet6Address.getByName("::127.0.0.1").getHostAddress());;
		
		
		System.out.println(String.format("%s|%s","127.0.0.1 ".trim(),"8080")); ;
		
		
		System.out.println(128 >> 7 & 1);
	}

}
