package myLearnjdk.jdk.net.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Date;

/**
 * udp 服务端
 * @author yangcheng  
 * @date 2020年8月18日  
 * @version V1.0
 */
public class UDPServer {
	public static void main(String[] args) {
		try(DatagramSocket socket = new DatagramSocket(13)){
			while(true){
				/**
				 * DatagramPacket中的字节数组用于缓存收到的字节数据
				 */
				DatagramPacket request = new DatagramPacket(new byte[1024], 1024);
				/**
				 * 该方法会阻塞执行
				 */
				socket.receive(request);
				String daytime = new Date().toString();
		        byte[] data = daytime.getBytes("UTF-8");
		        /**
		         * 根据接收到的数据报信息直接获取客户端信息，并将此信息封装到response数据报中
		         */
		        DatagramPacket response = new DatagramPacket(data, data.length, 
		                request.getAddress(), request.getPort());
	            socket.send(response);
	            System.out.println(daytime + " " + request.getAddress());
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
