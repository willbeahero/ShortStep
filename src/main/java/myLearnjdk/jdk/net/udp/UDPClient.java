package myLearnjdk.jdk.net.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * UDP客户端
 * @author yangcheng  
 * @date 2020年8月18日  
 * @version V1.0
 */
public class UDPClient {
	/**
	 * UDP发送数据，主要是将数据报DatagramPacket发送出去，目标服务器的IP和端口等
	 * 都是和被发送出去的数据一起被封装到DatagramPacket对象中
	 * DatagramSocket对象将DatagramPacket发送出去
	 * @param args
	 */
	public static void main(String[] args) {
		try(DatagramSocket socket = new DatagramSocket(0)){
			socket.setSoTimeout(10000);//设置超时很重要
			InetAddress host = InetAddress.getByName("127.0.0.1");
			
			DatagramPacket sendPacket = new DatagramPacket(new byte[1], 1, host,13);
			socket.send(sendPacket);
			/**
			 * 客户端接收数据
			 */
			DatagramPacket rcvPacket = new DatagramPacket(new byte[1024], 1024);
			socket.receive(rcvPacket);
			
			String str = new String(rcvPacket.getData(),0,rcvPacket.getLength(),"UTF-8");
			System.out.println(str);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
