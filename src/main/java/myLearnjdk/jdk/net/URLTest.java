package myLearnjdk.jdk.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * URL练习-URL可以通过new关键字创建，也可以通过	ClassLoader类加载文件并返回一个url
 * 目前URL支持的协议类型为：http/https等等
 * @author yangcheng  
 * @date 2020年7月29日  
 * @version V1.0
 */
public class URLTest {
	public static void main(String[] args) throws IOException {
		try {
			URL url = new URL("http://www.xianglong.work/");
//			System.out.println(url.getContent());
			URLConnection con = url.openConnection();
			System.out.println(con.getContentType());
			InputStream stream = con.getInputStream();
			
		} catch (MalformedURLException e) {
			/**
			 * 当当前字符串中指定的协议不被URL支持时，会报此异常
			 */
			e.printStackTrace();
		}
	}
}
