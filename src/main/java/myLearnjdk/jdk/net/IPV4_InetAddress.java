package myLearnjdk.jdk.net;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * InetAddress没有提供set方法，因此线程安全
 * @author yangcheng  
 * @date 2020年7月29日  
 * @version V1.0
 */
public class IPV4_InetAddress {
	public static void main(String[] args) throws IOException {
		InetAddress address = Inet4Address.getLocalHost();
		System.out.println(address.getHostAddress());//获取别名（域名也是别名）
		System.out.println(address.getLocalHost());
		System.out.println(address.getCanonicalHostName());//获取主机名（服务器主机名称）--会调用DNS
		
		
		InetAddress[] addressn = Inet4Address.getAllByName("47.104.100.254");
		System.out.println(addressn[0].getCanonicalHostName());
		/**
		 * 判断网络是否可达
		 */
		System.out.println("网络是否可达："+addressn[0].isReachable(2000));
		
		
	}

}
