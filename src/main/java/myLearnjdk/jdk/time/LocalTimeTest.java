package myLearnjdk.jdk.time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * jdk8对  Date 和simpleformat 的优化
 * @author yangcheng  
 * @date 2019年12月27日  
 * @version V1.0
 */
public class LocalTimeTest {
	
	public static void main(String[] args) {
		/**
		 * time不包含年月日
		 */
		LocalTime time  = LocalTime.now();
		System.out.println(time.toString());
		/**
		 * date不包含时分秒
		 */
		LocalDate date  = LocalDate.now();
		System.out.println(date.toString());
		/**
		 * LocalDateTime包含年月日 时分秒
		 */
		LocalDateTime dateTime  = LocalDateTime.now();
		System.out.println(dateTime.toString());
		
		
		DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss"); 
		LocalDateTime dateTime2  = LocalDateTime.now();
		
		System.out.println(dateTime2.format(formater));
		
	}
	
	
}
