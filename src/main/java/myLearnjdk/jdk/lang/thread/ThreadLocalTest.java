package myLearnjdk.jdk.lang.thread;
/**
 * threadLocal练习
 * @author yangcheng  
 * @date 2020年5月21日  
 * @version V1.0
 */
public class ThreadLocalTest {
	public static void main(String[] args) {
		
		ThreadLocalCache localCache = new ThreadLocalCache();
		/**
		 * 主线程利用threadlocal缓存值 
		 */
		localCache.set();
		
		new Thread(() -> {
			/**
			 * 子线程利用threadlocal缓存值 
			 */
			localCache.set();
			
			System.out.println("子线程："+localCache.getLong());
			System.out.println("子线程："+localCache.getString());
			
		}).start();
		
		
		System.out.println("主线程："+localCache.getLong());
		System.out.println("主线程："+localCache.getString());
	}
	
	
}
/**
 * 
 * @author yangcheng  
 * @date 2020年5月21日  
 * @version V1.0
 */
class ThreadLocalCache{
	private ThreadLocal<String> local1 = new ThreadLocal<String>();
	private ThreadLocal<Long> local2 = new ThreadLocal<Long>();
	public void set(){
		local1.set(Thread.currentThread().getName());
		local2.set(Thread.currentThread().getId());
	}
	public String getString() {
		return local1.get();
	}
	public long getLong() {
		return local2.get();
	}
	     
	
}