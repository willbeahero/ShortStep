package myLearnjdk.jdk.lang;
/**
 * String中的工具类使用
 * @author yangcheng  
 * @date 2019年9月26日  
 * @version V1.0
 */
public class StringTest {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println(byte2HexString((byte)92));
		
	}
	
	
	
	/**
	 * 字节转16进制字符串
	 * @param data
	 * @return
	 */
	private static String byte2HexString(byte data){
		String hex = Integer.toHexString(data & 0xFF);
		return hex.length() < 2 ? '0'+hex : hex;
	}
	
	/**
	 * 16进制字符串转字节
	 * @param data
	 * @return
	 */
	private static byte hexString2Byte(String data){
		return (byte)Integer.parseInt(data, 16);
	}

}
