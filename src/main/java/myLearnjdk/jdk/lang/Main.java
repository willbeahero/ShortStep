package myLearnjdk.jdk.lang;

import java.io.IOException;
import java.util.Scanner;

public class Main {
	public static void main(String[] arg) throws IOException{
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {//注意while处理多个case              int a = in.nextInt();
             try{
                 String sre = in.nextLine().toString().trim();
                if(sre.indexOf(".") > 0 ){
                    String[] ips = sre.split("\\.");
                    if(ips.length != 4){
                        System.out.println("NO");
                    }else{
                    	boolean sig = true;
                        for(int i = 0 ; i < 4 ; i ++){
                        	if(Integer.parseInt(ips[i]) < 0){
                                System.out.println("NO");
                                sig = false;
                                break;
                            }
                        }
                        if(sig){
                        	System.out.println("YES");
                        }
                    }
                }else{
                    System.out.println("NO");
                }
            }catch(Exception e){
                System.out.println("NO");
            }
        }
    }
}
