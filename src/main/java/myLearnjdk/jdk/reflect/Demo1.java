package myLearnjdk.jdk.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
/**
 * 获取方法参数和返回值类型中对应的泛型值
 * @author yangcheng  
 * @date 2019年1月23日  
 * @version V1.0
 */
public class Demo1 {
	public void test01(Map<String,User> map,List<User> list){
		System.out.println("Demo1.test01()");
	}	
	public Map<Integer,User> test02(){
		System.out.println("Demo1.test02()");
		return null;
	}	
	public static void main(String[] args) {
		try {			
			//获得指定方法参数泛型信息
			Method m = Demo1.class.getMethod("test01", Map.class,List.class);
			Type[] t = m.getGenericParameterTypes();
			for (Type paramType : t) {
				System.out.println("#"+paramType);
				if(paramType instanceof ParameterizedType){// ParameterizedType表示带有泛型的参数
					Type[] genericTypes = ((ParameterizedType) paramType).getActualTypeArguments();
					for (Type genericType : genericTypes) {
						System.out.println("泛型类型："+genericType);
					}
				}
			}			
			
			
			//获得指定方法返回值泛型信息
			Method m2 = Demo1.class.getMethod("test02", null);
			Type returnType = m2.getGenericReturnType();
			if(returnType instanceof ParameterizedType){//returnType=java.util.Map<java.lang.Integer, dynamicjava.reflect.User>
					Type[] genericTypes = ((ParameterizedType) returnType).getActualTypeArguments();

					for (Type genericType : genericTypes) {
						System.out.println("返回值，泛型类型："+genericType);
					}					 
			}		
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}


}

class User{
	
}
