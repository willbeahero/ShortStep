package myLearnjdk.jdk.reflect;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.List;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * java引擎执行脚本
 * @author yangcheng  
 * @date 2019年1月23日  
 * @version V1.0
 */
public class ScriptEngineTest {
	
	public static void main(String[] args) throws ScriptException, NoSuchMethodException, FileNotFoundException {
		//获得脚本执行引擎对象
		ScriptEngineManager engineManager = new ScriptEngineManager();
		ScriptEngine scriptEngine=engineManager.getEngineByName("javascript");
		//定义变量并存储到引擎上下文中
		scriptEngine.put("msg", "jerry");
		System.out.println("msg="+scriptEngine.get("msg"));
		//通过字符串定义脚本  并通过引擎的eval()方法执行。
		
//		String str = " var man ={name : 'yc' , age : 20 }; " ;
//		str += "println(man.name);" ;
//		scriptEngine.eval(str);
		
		//直接通过js也可以修改引擎对象上下文中变量的值
		String str1 = "msg = 'tom' ;";
		scriptEngine.eval(str1);
		System.out.println("msg="+scriptEngine.get("msg"));
		
		/**
		 * 定义函数并执行
		 */
		//1.定义函数
		String str2 = "function add (a , b) { return a+b; } ";
		scriptEngine.eval(str2);
		//2.取得调用接口
		Invocable jsinvocable =(Invocable) scriptEngine;
		//3.执行脚本中定义的方法
		Object result=jsinvocable.invokeFunction("add", 1,2);
		System.out.println("result="+result);
		
		/**
		 * js中引入java包 并使用相关类
		 */
//		String str3="importPackage(java.util); var list = Arrays.asList(['a','v','c']);";
//		scriptEngine.eval(str3);
//		List list=(List) scriptEngine.get("list");
//		for (Object object : list) {
//			System.out.println(object);
//		}
		//直接加载.js文件    并执行js中的f1方法  同时获取返回值
		URL url=Demo1.class.getClassLoader().getResource("myLearnjdk/jdk/reflect/assist/test.js");
		scriptEngine.eval(new FileReader(url.getPath()));
		Invocable jsinvocable2 =(Invocable) scriptEngine;
		System.out.println(jsinvocable2.invokeFunction("f1",new byte[]{2,8}));;
	}


}
