package myLearnjdk.jdk.arithmetic.Spearman;

import java.util.Random;

public class FisherYatesShuffle {
	public static int[] shuffle(int[] array) {
		for (int i = array.length - 1; i > 0; i--) {
			int rand = (new Random()).nextInt(i+1);
			int temp = array[i];
			array[i] = array[rand];
			array[rand] = temp;
		}
		return array;
	}
	
	public static void main(String[] args) {
		
		int[] array1= {1, 2, 3, 4, 5};
		int[] array2 = shuffle(array1);
		for(int elem: array2)
			System.out.print(elem + " ");
		System.out.println();
	}
}
