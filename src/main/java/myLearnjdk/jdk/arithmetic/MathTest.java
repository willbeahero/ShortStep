package myLearnjdk.jdk.arithmetic;
/**
 * 
 * @author yangcheng  
 * @date 2020年11月22日  
 * @version V1.0
 */
public class MathTest {

	public static void main(String[] args) {
//		test001(8);
		test002(1026);
	}
	
	public static void test001(int directMemoryCacheAlignment){
//		boolean flag = (directMemoryCacheAlignment & -directMemoryCacheAlignment) != directMemoryCacheAlignment;
		int i = -directMemoryCacheAlignment;
		int j = directMemoryCacheAlignment & i;
		boolean flag =  j != directMemoryCacheAlignment;
		System.out.println("是不是2的倍数"+flag);
	}
	/**
	 * -计算pageSize是2的多少次。
	 * @param directMemoryCacheAlignment
	 */
	public static void test002(int pageSize){
		int i = Integer.SIZE - 1 - Integer.numberOfLeadingZeros(pageSize);
		System.out.println("pageSize为2的"+i +"次方");
	}
	
	
	
}
