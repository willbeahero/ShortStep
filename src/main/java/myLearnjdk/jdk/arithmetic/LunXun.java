package myLearnjdk.jdk.arithmetic;
/**
 * 轮询策略算法
 * @author yangcheng  
 * @date 2019年3月14日  
 * @version V1.0
 */
public class LunXun {
	static volatile int index = 0; // 索引:指定起始位置
	public static void main(String[] args) {
		int[] arr = { 4, 3, 2, 1, 0 };
		
		
	
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 0; i < 30; i++) {
					int nextIndex = (index + 1) % arr.length;
					index = nextIndex;
					System.out.println(Thread.currentThread().getName()+arr[index] + " ,index=" + index);
				}
			}
		},"11111").start();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 0; i < 30; i++) {
					int nextIndex = (index + 1) % arr.length;
					index = nextIndex;
					System.out.println(Thread.currentThread().getName()+arr[index] + " ,index=" + index);
				}
			}
		},"22222").start();
	}
}
