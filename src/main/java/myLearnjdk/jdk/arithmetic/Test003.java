package myLearnjdk.jdk.arithmetic;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
/**
 * 模拟一个连续的内存块，大小为固定的100字节
 * 1.用户输入"REQUEST=10"表示从内存中分配一个连续的大小为10字节的空间，当分配
 * 成功时，输出内存区块的首地址，分配失败(内存空间不足)时输出"error"
 * 2.用户输入“RELEASE=0”,表示释放首地址为0所对应的区块，如果不存在相应区块，则输出“error”,反之不输出
 * 
 * @author yangcheng  
 * @date 2020年6月30日  
 * @version V1.0
 */
public class Test003 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int questPos = 0 ; //申请指针
		int releasePos = 0 ;//释放指针
		int[] cache = new int[100]; 
		for(int i = 0 ; i < 100 ; i ++){
			cache[i] = 0 ;
		}
		
		Map<Integer,Integer> requesrRcord = new HashMap<>();
		while (in.hasNextLine()) {
			 String sre = in.nextLine().toString().trim();
			 if(sre.startsWith("REQ")){
				 //申请
				 int size = Integer.parseInt(sre.split("\\=")[1]);
				 if(size == 0){
					 System.out.println("error");
				 }else if(questPos + size <= 100){
					//没有释放过
					 requesrRcord.put(questPos, size);//成功分配时记录分配信息用于释放
					 System.out.println(questPos);
					 int cap = questPos+size;
					 for(int i = questPos ;  i < cap ;i++ , questPos ++ ){
						 cache[i] = 1;
					 }
					 
					 
				 }else if(questPos + size > 100  && releasePos != 0){
					 //执行过释放,则从0开始遍历寻找一个连续尺寸为size的内存区块
					 int linkSize = 0 ;//连续区间大小
					 int linkPos = -1 ;//连续区间内存地址
					 for(int i = 0 ; i < releasePos ;i++){
						 if(cache[i] == 0 ){
							 ++linkSize;
							 if(linkSize == size){
								 //已经找到可以分配的空间
								 break;
							 }
						 }else{
							 //为1时表名当前地址已占用
							 linkSize = 0 ;
							 linkPos = i;
						 }
					 }
					 if(linkSize >= size){
						 requesrRcord.put(linkPos +1, size);//成功分配时记录分配信息用于释放
						 System.out.println(linkPos +1);
						 for(int i = (linkPos +1);i< (linkPos +1+size) ; i ++){//
							 //分配地址
							 cache[i] = 1;
						 }
					 }else{
						 //不存在这么大的连续内存块了
						 System.out.println("error");
					 }
					 
					 
				 }else{
					 System.out.println("error");
				 }
				 
				 
			 }else if(sre.startsWith("REL")){
				 //释放
				 Integer addr = Integer.parseInt(sre.split("\\=")[1]);//释放地址
				 if(requesrRcord.containsKey(addr)){
					 int capSize = requesrRcord.get(addr);
					 
					 for(int i = addr ; i <  capSize ; i++){
						 cache[i] = 0;
						 releasePos = addr + capSize;
					 }
				 }else{
					 System.out.println("error");//不存在
				 }
				 
			 }
		}
	}
}
