package myLearnjdk.jdk.common.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

/**
 * 命令行客户端测试
 * @author yangcheng  
 * @date 2019年3月14日  
 * @version V1.0
 */
public class OptionsTest {
	
	public static void init(String[] args) throws ParseException{
		//CLI 定义阶段
		Options options = new Options();
		Option op = new Option("a", true, "测试参数1");
		op.setRequired(true);
		//CLI 解析阶段
		CommandLineParser parser = new PosixParser(); 
		 CommandLine cmd = parser.parse(options, args); 
		// CLI 询问阶段
		 Option[] opts = cmd.getOptions();
         if (opts != null) {
             for (Option opt1 : opts) {
                 String name = opt1.getLongOpt();
                 String value = cmd.getOptionValue(name);
                 System.out.println(name + "=>" + value);
             }
         }
	}
	public static void main(String[] args) throws ParseException {
		
		//CLI 定义阶段
		Options options = new Options();
		Option op = new Option("a", true, "测试参数1");
		op.setRequired(true);
		options.addOption(op);
		//CLI 解析阶段
		CommandLineParser parser = new PosixParser(); 
		 CommandLine cmd = parser.parse(options, args); 
		// CLI 询问阶段
		 Option[] opts = cmd.getOptions();
         if (opts != null) {
             for (Option opt1 : opts) {
                 String name = opt1.getOpt();
                 String value = cmd.getOptionValue(name);
                 System.out.println(name + "=>" + value);
             }
         }
		
		
	}
	
	
}
