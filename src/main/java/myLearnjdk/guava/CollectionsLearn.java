package myLearnjdk.guava;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableMap;

/**
 * guava得集合
 * @author BriansPC
 *
 */
public class CollectionsLearn {
	public static void main(String[] args) {
		immutableColections();
	}
	/**
	 * 不可变集合
	 * 不可变集合向较于普通得hashmap和ConcurrentHashMap在只读情况下得效率比较总结：
	 * 
	 * ConcurrentHashMap与HashMap在不同数量级上性能相差无几，ConcurrentHashMap以十分微弱得优势取胜
	 * 但是ImmutableMap随着访问数量级得提升，性能优势越发明显，其最终性能亿级访问时 效率几乎是前二者得2倍左右！
	 * 
	 * ----jdk自带得Collections.unmodifiableMap()实现得不可变map  性能最低！一亿308ms
	 * 
	 */
	public static void immutableColections(){
		ImmutableMap<String, String> guavamap = new ImmutableMap.Builder<String,String>().build();//遍历100万次  7ms  1000万18ms  一亿135ms
		
		Map<String,String> map = new HashMap<>();//遍历100万次  9ms   1000万34ms  一亿284ms
		
//		ConcurrentHashMap<String, String> map= new ConcurrentHashMap<>();//遍历100万次  9ms  1000万33ms  一亿278ms
		
		
		
		for(int i=0;i<10;i++){
			map.put(String.valueOf(i), String.valueOf(i));
		}
		guavamap.copyOf(map);//1.先实例化然后才可以执行
//		ImmutableMap.copyOf(map);//2.实例化ImmutableMap得第二种方式
//		Map<String,String> map2 =Collections.unmodifiableMap(map);//jdk 自带得不可变集合实现   一亿308ms，效率最低
		
		long s = System.currentTimeMillis();
		for(int i = 0 ;i<100000000 ; i++){
			map.get(i%10);
			
		}
		long e = System.currentTimeMillis();
		
		System.out.println("共耗时"+(e-s)+"ms");
	}
	
	
	/**
	 * BiMap得作用是一个map得key 和value可以反转
	 */
	public static void testBiMap(){
		BiMap<String, Integer> userId = HashBiMap.create();
		for(int i=0;i<10;i++){
			userId.put(String.valueOf(i), i);
		}
		
		long s = System.currentTimeMillis();
		for(int i = 0 ;i<100000000 ; i++){
			userId.inverse().get(i%10);
		}
		long e = System.currentTimeMillis();
		
		System.out.println("共耗时"+(e-s)+"ms");
	}
	
	
	/**
	 * table---逻辑表格
	 */
	/**
	 * 
	 */

}
