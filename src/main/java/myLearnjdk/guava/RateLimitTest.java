package myLearnjdk.guava;

import com.google.common.util.concurrent.RateLimiter;

/**
 * 令牌桶限流
 * @author yangcheng  
 * @date 2020年5月21日  
 * @version V1.0
 */
public class RateLimitTest {
	
	public static void main(String[] args) {
		//控制令牌每秒产生1个
		RateLimiter rateLimiter = RateLimiter.create(5);
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				for(;;){
					if(rateLimiter.tryAcquire()){
						System.out.println("获取到令牌");
					}else{
						try {
							Thread.sleep(5002);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				
			}
		}).start();
		
	}

}
