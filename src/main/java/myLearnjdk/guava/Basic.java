package myLearnjdk.guava;

import java.util.Objects;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

/**
 * guava学习  http://ifeve.com/google-guava/
 * 基本工具
 * @author yangcheng  
 * @date 2018年12月12日  
 * @version V1.0
 */
public class Basic {
	public static void main(String[] args) {
		optionnal();
		
		checkArgument(1, 2);
		
	}
	
	/**
	 * Optional处理null值
	 */
	public static void optionnal(){
		/**
		 * 可以将方法得返回值定义为一个Optional<T>,这样可以让调用方法得人员处理为空得情况
		 */
		
		String str = "";
//		Optional<String> optional = Optional.of(str);//当str为null直接报空
		Optional<String> optional = Optional.fromNullable(str);
		if(optional.isPresent()){//isPresent()判断是否为空
			System.out.println("optional=="+optional.get());//get()返回optional中封装得str得值
		}
		/**
		 * 当str为null时，会用or()方法得参数作为str得值返回
		 */
		String optional2 = Optional.of(str).or("代替");
		System.out.println("optional2=="+optional.get());//get()返回optional中封装得str得值
		
	} 
	
	/**
	 * checkArgument  前置条件判断,使用得是Preconditions类中提供得所有方法
	 */
	public static void checkArgument(Integer i , Integer j ){
		/**
		 * 错误信息描述信息类似于printf，不过占位符只有%s一个
		 */
//		Preconditions.checkArgument(i > j, "当不满足条件得时候会报出当前指定得错误信息%s，如果不设置该提示得话 则没有错误提示", i);
		Preconditions.checkNotNull(i, "为null时报错");
	}

}
